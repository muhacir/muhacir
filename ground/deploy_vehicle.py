import time
import click
import dronekit
from pymavlink import mavutil


# main function
@click.command()
@click.option("--vehicle", help="ID of the vehicle", required=True, type=int)
def main(vehicle):
    # assign vehicle number
    vehicle_number = int(vehicle)

    # vehicle connection path
    path_vehicle_connection = "udp:192.168.1.200:" + str(9000 + vehicle_number * 10)

    # connect to vehicle
    vehicle = dronekit.connect(path_vehicle_connection)
    print("connected")

    # get into the GUIDED mode
    while not vehicle.mode == "GUIDED":
        vehicle.mode = dronekit.VehicleMode("GUIDED")
        time.sleep(0.3)
    print("guided")

    # wait for armable
    while not vehicle.is_armable:
        time.sleep(0.3)
    print("armable")

    # arm the vehicle
    while not vehicle.armed:
        vehicle.armed = True
        time.sleep(0.3)
    print("armed")

    # get into the AUTO mode
    while not vehicle.mode == "AUTO":
        vehicle.mode = dronekit.VehicleMode("AUTO")
        time.sleep(0.3)
    print("auto")

    # send mission start
    vehicle.send_mavlink(vehicle.message_factory.command_long_encode(0, 0,
                                                                     mavutil.mavlink.MAV_CMD_MISSION_START, 0, 0, 0, 0,
                                                                     0, 0, 0, 0))
    print("deployed")


# main function
if __name__ == '__main__':
    main()
