import geopy.distance
import geographiclib.geodesic
import locations

# configurations
uav_number = 5
location_takeoff = locations.location_takeoff
location_approach = locations.location_approach
location_land = locations.location_land
location_field = locations.area_upper
distance_separation_line = 50.0
distance_separation_area = 50.0
distance_turn_around = 100.0
flight_level_default = 60.0
flight_level_separation = 5.0
flight_level_travel = (flight_level_default + flight_level_separation * 5,
                       flight_level_default + flight_level_separation * 4,
                       flight_level_default + flight_level_separation * 3,
                       flight_level_default + flight_level_separation * 2,
                       flight_level_default + flight_level_separation * 1)
flight_level_approach = (flight_level_default / 2.0 + flight_level_separation * 5,
                         flight_level_default / 2.0 + flight_level_separation * 4,
                         flight_level_default / 2.0 + flight_level_separation * 3,
                         flight_level_default / 2.0 + flight_level_separation * 2,
                         flight_level_default / 2.0 + flight_level_separation * 1)
time_loiter_after_search = 60 * 10
time_loiter_target_detection = 30
radius_loiter_turn = -120.0
area_share_order = (5, 3, 1, 2, 4)


# calculates midpoint between two locations
def calculate_midpoint_line(locations):
    # calculate bearing
    bearing = geographiclib.geodesic.Geodesic.WGS84.Inverse(locations[0][0], locations[0][1],
                                                            locations[1][0], locations[1][1])["azi1"]
    bearing = (bearing + 360.0) % 360.0

    # calculate distance
    distance = geopy.distance.distance(locations[0], locations[1]).meters

    # calculate midpoint
    midpoint = geopy.distance.distance(meters=distance / 2.0).destination(locations[0], bearing)

    # expose midpoint
    return midpoint


def calculate_midpoint_area(locations):
    # calculate first midpoint
    midpoint_first = calculate_midpoint_line(locations[:2])

    # calculate second midpoint
    midpoint_second = calculate_midpoint_line(locations[-2:])

    # calculate final midpoint
    midpoint = calculate_midpoint_line((midpoint_first, midpoint_second))

    # expose midpoint
    return midpoint


# show takeoff location
print("Takeoff Location > Latitude:{0} Longitude:{1}".format(location_takeoff[0], location_takeoff[1]))

# calculate bearing for upper field locations
bearing = geographiclib.geodesic.Geodesic.WGS84.Inverse(location_field[0][0], location_field[0][1],
                                                        location_field[1][0], location_field[1][1])["azi1"]
bearing = (bearing + 360.0) % 360.0

# extend upper field locations with distance turn around
extended_upper_left = geopy.distance.distance(meters=distance_turn_around).destination(location_field[0],
                                                                                       (bearing + 180.0) % 360.0)
extended_upper_right = geopy.distance.distance(meters=distance_turn_around).destination(location_field[1], bearing)

# calculate bearing for lower field locations
bearing = geographiclib.geodesic.Geodesic.WGS84.Inverse(location_field[3][0], location_field[3][1],
                                                        location_field[2][0], location_field[2][1])["azi1"]
bearing = (bearing + 360.0) % 360.0

# extend upper field locations with distance turn around
extended_lower_left = geopy.distance.distance(meters=distance_turn_around).destination(location_field[3],
                                                                                       (bearing + 180.0) % 360.0)
extended_lower_right = geopy.distance.distance(meters=distance_turn_around).destination(location_field[2], bearing)

# change locations field array with new corners
location_field = ((extended_upper_left.latitude, extended_upper_left.longitude),
                  (extended_upper_right.latitude, extended_upper_right.longitude),
                  (extended_lower_right.latitude, extended_lower_right.longitude),
                  (extended_lower_left.latitude, extended_lower_left.longitude))

# show corner locations
for i, location in enumerate(location_field):
    print("Field Location {0} > Latitude:{1} Longitude:{2}".format(i, location[0], location[1]))

# calculate bearing
bearing = geographiclib.geodesic.Geodesic.WGS84.Inverse(location_field[0][0], location_field[0][1],
                                                        location_field[3][0], location_field[3][1])["azi1"]
bearing = (bearing + 360.0) % 360.0

# calculate distance
distance = geopy.distance.distance(location_field[0], location_field[3]).meters
distance_delta = distance / (uav_number - 1)

# calculate start points
points_start = []
for i in range(uav_number + 1):
    midpoint = geopy.distance.distance(meters=distance_delta * i).destination(location_field[0], bearing)
    points_start.append(midpoint)

# calculate bearing
bearing = geographiclib.geodesic.Geodesic.WGS84.Inverse(location_field[1][0], location_field[1][1],
                                                        location_field[2][0], location_field[2][1])["azi1"]
bearing = (bearing + 360.0) % 360.0

# calculate distance
distance = geopy.distance.distance(location_field[1], location_field[2]).meters
distance_delta = distance / (uav_number - 1)

# calculate end points
points_end = []
for i in range(uav_number + 1):
    midpoint = geopy.distance.distance(meters=distance_delta * i).destination(location_field[1], bearing)
    points_end.append(midpoint)

# calculate line bearings
bearing1 = geographiclib.geodesic.Geodesic.WGS84.Inverse(points_start[0][0], points_start[0][1],
                                                         points_end[0][0], points_end[0][1])["azi1"]
bearing1 = (bearing1 + 360.0) % 360.0
bearing2 = geographiclib.geodesic.Geodesic.WGS84.Inverse(points_start[-1][0], points_start[-1][1],
                                                         points_end[-1][0], points_end[-1][1])["azi1"]
bearing2 = (bearing2 + 360.0) % 360.0

# sanity check for calculations are correct
if abs(bearing1 - bearing2) > 5.0:
    print("Reverse detected, exiting...")
    exit(1)

# show area slicer lines
for i, point in enumerate(points_start):
    print("Line:", i, "> from",
          "Latitude:{0:.6f}".format(point.latitude), "Longitude:{0:.6f}".format(point.longitude), "to",
          "Latitude:{0:.6f}".format(points_end[i].latitude), "Longitude:{0:.6f}".format(points_end[i].longitude))

# create sub area corners
locations_subfield = []
for i in range(uav_number):
    locations_subfield.append(((points_start[i].latitude, points_start[i].longitude),
                               (points_end[i].latitude, points_end[i].longitude),
                               (points_end[i + 1].latitude, points_end[i + 1].longitude),
                               (points_start[i + 1].latitude, points_start[i + 1].longitude)))
locations_subfield = tuple(locations_subfield)

# show sub area corners
for i, field in enumerate(locations_subfield):
    for j, location in enumerate(field):
        print("Field:", i, ">", "Corner:", j, ">",
              "Latitude:{0:.6f}".format(location[0]), "Longitude:{0:.6f}".format(location[1]))

# create sub area center locations
subfield_centers = []
for field in locations_subfield:
    midpoint = calculate_midpoint_area(field)
    midpoint = (midpoint.latitude, midpoint.longitude)
    subfield_centers.append(midpoint)

# show sub area centers
for i, location in enumerate(subfield_centers):
    print("Sub Field Center Location {0} > Latitude:{1} Longitude:{2}".format(i, location[0], location[1]))

# calculate waypoints
waypoints = []
for field in locations_subfield:
    waypoints.append([])
    bearing = geographiclib.geodesic.Geodesic.WGS84.Inverse(field[0][0], field[0][1], field[3][0], field[3][1])["azi1"]
    bearing = (bearing + 360.0) % 360.0

    i = -1
    while True:
        i += 1
        location_start = geopy.distance.distance(meters=distance_separation_line * i).destination(field[0], bearing)
        location_end = geopy.distance.distance(meters=distance_separation_line * i).destination(field[1], bearing)
        line_distance = geopy.distance.distance(location_start, field[3]).meters
        if i % 2 != 1:
            waypoints[-1].append((location_start.latitude, location_start.longitude))
            waypoints[-1].append((location_end.latitude, location_end.longitude))
        else:
            waypoints[-1].append((location_end.latitude, location_end.longitude))
            waypoints[-1].append((location_start.latitude, location_start.longitude))

        if line_distance < distance_separation_line:
            break

# print calculated waypoints
for j in range(len(waypoints)):
    path_length = 0.0
    for i in range(1, len(waypoints[j])):
        path_length += geopy.distance.GeodesicDistance(waypoints[j][i], waypoints[j][i - 1]).meters
    # path_length += distance.GeodesicDistance(waypoints[j][-1], land_points[j]).meters
    print("UAV: " + str(j + 1) + " Total Distance: " + str(path_length))

# generate waypoint files
for i in range(uav_number):

    # initial navigation plan line
    text_list = ["QGC WPL 110\n", "0	1	0	0	0	0	0	0	0	0	0	1\n"]

    # [MAV_CMD_NAV_TAKEOFF = 22] takeoff to travel flight level
    waypoint = "1   0	3	22	0.00000000	0.00000000	0.00000000	0.00000000	0.00000000	0.00000000  {0:.8f} 1\n"
    waypoint = waypoint.format(flight_level_travel[i])
    text_list.append(waypoint)

    # [MAV_CMD_NAV_WAYPOINT = 16] go to first location of the sub search area
    waypoint = "2   0   3   16  0.00000000  0.00000000  0.00000000  0.00000000  {0:.8f} {1:.8f} {2:.8f} 1\n"
    waypoint = waypoint.format(waypoints[i][0][0], waypoints[i][0][1], flight_level_travel[i])
    text_list.append(waypoint)

    # [MAV_CMD_NAV_LOITER_TO_ALT = 31] lower the altitude to search altitude
    waypoint = "3   0   3   31  0.00000000  0.00000000  0.00000000  0.00000000  {0:.8f} {1:.8f} {2:.8f} 1\n"
    waypoint = waypoint.format(waypoints[i][0][0], waypoints[i][0][1], flight_level_default)
    text_list.append(waypoint)

    # [MAV_CMD_NAV_WAYPOINT = 16] create search waypoints
    for j in range(20):
        waypoint = "{0} 0   3   16  0.00000000  0.00000000  0.00000000  0.00000000  {1:.8f} {2:.8f} {3:.8f} 1\n"
        waypoint = waypoint.format(j + 4, waypoints[i][j][0], waypoints[i][j][1], flight_level_default)
        text_list.append(waypoint)

    # [MAV_CMD_NAV_LOITER_TIME = 19] go to center of the sub search field and wait time_loiter_after_search seconds
    waypoint = "{0} 0   3   19  {1:.8f}  0.00000000  0.00000000  0.00000000  {2:.8f} {3:.8f} {4:.8f} 1\n"
    waypoint = waypoint.format(j + 5, time_loiter_after_search, subfield_centers[i][0], subfield_centers[i][1],
                               flight_level_travel[i])
    text_list.append(waypoint)

    # [MAV_CMD_NAV_LOITER_TO_ALT = 31] lower the altitude to search altitude
    waypoint = "{0} 0   3   31  0.00000000  0.00000000  0.00000000  0.00000000  {1:.8f} {2:.8f} {3:.8f} 1\n"
    waypoint = waypoint.format(j + 6, subfield_centers[i][0], subfield_centers[i][1], flight_level_default)
    text_list.append(waypoint)

    # [MAV_CMD_NAV_LOITER_TURNS = 18] go and loiter above the target with 1 turn
    waypoint = "{0} 0   3   18  1.00000000  0.00000000  {1:.8f}  0.00000000  {2:.8f} {3:.8f} {4:.8f} 1\n"
    waypoint = waypoint.format(j + 7, radius_loiter_turn, subfield_centers[i][0], subfield_centers[i][1],
                               flight_level_default)
    text_list.append(waypoint)

    # [MAV_CMD_NAV_LOITER_TURNS = 18] go and loiter above the target with 1 turn
    waypoint = "{0} 0   3   18  1.00000000  0.00000000  {1:.8f}  0.00000000  {2:.8f} {3:.8f} {4:.8f} 1\n"
    waypoint = waypoint.format(j + 8, radius_loiter_turn, subfield_centers[i][0], subfield_centers[i][1],
                               flight_level_default)
    text_list.append(waypoint)

    # [MAV_CMD_NAV_LOITER_TURNS = 18] go and loiter above the target with 1 turn
    waypoint = "{0} 0   3   18  1.00000000  0.00000000  {1:.8f}  0.00000000  {2:.8f} {3:.8f} {4:.8f} 1\n"
    waypoint = waypoint.format(j + 9, radius_loiter_turn, subfield_centers[i][0], subfield_centers[i][1],
                               flight_level_default)
    text_list.append(waypoint)

    # [MAV_CMD_NAV_LOITER_TURNS = 18] go and loiter above the target with 1 turn
    waypoint = "{0} 0   3   18  1.00000000  0.00000000  {1:.8f}  0.00000000  {2:.8f} {3:.8f} {4:.8f} 1\n"
    waypoint = waypoint.format(j + 10, radius_loiter_turn, subfield_centers[i][0], subfield_centers[i][1],
                               flight_level_default)
    text_list.append(waypoint)

    # [MAV_CMD_NAV_LOITER_TO_ALT = 31] lower altitude to approach altitude because going to approach location
    waypoint = "{0} 0   3   31  0.00000000  0.00000000  0.00000000  0.00000000  0.00000000 0.00000000 {1:.8f} 1\n"
    waypoint = waypoint.format(j + 11, flight_level_approach[i])
    text_list.append(waypoint)

    # [MAV_CMD_NAV_WAYPOINT = 16] go to the approach location
    waypoint = "{0} 0   3   16  0.00000000  0.00000000  0.00000000  0.00000000  {1:.8f} {2:.8f} {3:.8f} 1\n"
    waypoint = waypoint.format(j + 12, location_approach[0], location_approach[1], flight_level_approach[i])
    text_list.append(waypoint)

    # [MAV_CMD_NAV_LAND = 21] land to the landing zone
    waypoint = "{0} 0   3   21  0.00000000  0.00000000  0.00000000  0.00000000  {1:.8f} {2:.8f} 0.00000000 1\n"
    waypoint = waypoint.format(j + 13, location_land[0], location_land[1])
    text_list.append(waypoint)

    # save waypoint lines to file
    with open("missions/{0}.waypoints".format(area_share_order[i]), "w") as out_file:
        out_file.writelines(text_list)
