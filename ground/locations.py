area_upper = ((38.4208750154, 33.7171225264),  # upper left
              (38.4133102912, 33.7379071764),  # upper right
              (38.3959894005, 33.7316025888),  # lower right
              (38.4035530860, 33.7108222758))  # lower left

area_lower_left = ((38.4077753497, 33.6850348262),  # upper left
                   (38.4002110739, 33.7058159979),  # upper right
                   (38.3828903146, 33.6995117925),  # lower right
                   (38.3904522056, 33.6787341641))  # lower left

area_lower_right = ((38.3984746985, 33.7064925859),  # upper left
                    (38.3909105887, 33.7272711997),  # upper right
                    (38.3735895719, 33.7209669007),  # lower right
                    (38.3811500371, 33.7001910883))  # lower left

location_takeoff = (38.397373, 33.711180)

location_land = tuple(location_takeoff)

location_approach = (38.3954747, 33.7177384)
