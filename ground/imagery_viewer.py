import os
import time
import pprint
import cv2
import piexif
import piexif.helper
import json

# configurations
path_imagery_global = "/home/m/muhacir/imagery_data/"
slow_down = True
delay_loop = 1
osd_font = cv2.FONT_HERSHEY_DUPLEX
osd_color = (0, 255, 0)
osd_scale = 1
osd_thickness = 2


# exif dumper convenience function
def telemetry_data_reader(file_name):
    # try to read exif data
    try:

        # read the exif data
        exif_dict = piexif.load(file_name)

        # extract the serialized data
        user_comment = piexif.helper.UserComment.load(exif_dict["Exif"][piexif.ExifIFD.UserComment])

        # deserialize
        user_data = json.loads(user_comment)
        user_data = dict(user_data)

    # exception occurred
    except Exception as e:

        # print the error
        print(e)

        # fall back to empty dictionary
        user_data = {}

    # expose data
    return user_data


# show exif data on image
def telemetry_writer(frame, data):
    frame_copy = frame.copy()
    cv2.putText(frame_copy, "LAT: " + "{0:.6f}".format(data["lat"]), (10, 30),
                osd_font, osd_scale, osd_color, osd_thickness)
    cv2.putText(frame_copy, "HDG: " + "{0:.1f}".format(data["hdg"]), (600, 30),
                osd_font, osd_scale, osd_color, osd_thickness)
    cv2.putText(frame_copy, "TIM: " + data["tim"].split(" ")[1], (1050, 30),
                osd_font, osd_scale, osd_color, osd_thickness)
    cv2.putText(frame_copy, "LON: " + "{0:.6f}".format(data["lon"]), (10, 60),
                osd_font, osd_scale, osd_color, osd_thickness)
    cv2.putText(frame_copy, "MOD: " + data["mod"], (1050, 60),
                osd_font, osd_scale, osd_color, osd_thickness)
    cv2.putText(frame_copy, "ALT: " + "{0:.1f}".format(data["alt"]), (10, 90),
                osd_font, osd_scale, osd_color, osd_thickness)
    cv2.putText(frame_copy, "ARM: " + str(data["arm"]).upper(), (1050, 90),
                osd_font, osd_scale, osd_color, osd_thickness)
    cv2.putText(frame_copy, "RLL: " + "{0:.1f}".format(data["rll"]), (10, 150),
                osd_font, osd_scale, osd_color, osd_thickness)
    cv2.putText(frame_copy, "BTM: " + "{:06d}".format(data["btm"]), (1050, 150),
                osd_font, osd_scale, osd_color, osd_thickness)
    cv2.putText(frame_copy, "PTC: " + "{0:.1f}".format(data["ptc"]), (10, 180),
                osd_font, osd_scale, osd_color, osd_thickness)
    cv2.putText(frame_copy, "BAT: " + "{0:.1f}".format(data["bat"]), (1050, 180),
                osd_font, osd_scale, osd_color, osd_thickness)
    cv2.putText(frame_copy, "YAW: " + "{0:.1f}".format(data["yaw"]), (10, 210),
                osd_font, osd_scale, osd_color, osd_thickness)
    cv2.putText(frame_copy, "HBT: " + "{0:.1f}".format(data["hbt"]), (1050, 210),
                osd_font, osd_scale, osd_color, osd_thickness)
    cv2.putText(frame_copy, "ASP: " + "{0:.1f}".format(data["asp"]), (10, 270),
                osd_font, osd_scale, osd_color, osd_thickness)
    cv2.putText(frame_copy, "VLT: " + "{0:.1f}".format(data["vlt"]), (1050, 270),
                osd_font, osd_scale, osd_color, osd_thickness)
    cv2.putText(frame_copy, "GSP: " + "{0:.1f}".format(data["gsp"]), (10, 300),
                osd_font, osd_scale, osd_color, osd_thickness)
    cv2.putText(frame_copy, "CUR: " + "{0:.1f}".format(data["cur"]), (1050, 300),
                osd_font, osd_scale, osd_color, osd_thickness)
    cv2.putText(frame_copy, "SID: " + str(data["sid"]), (10, 360),
                osd_font, osd_scale, osd_color, osd_thickness)
    cv2.putText(frame_copy, "FRM: " + "{:06d}".format(data["frm"]), (1050, 360),
                osd_font, osd_scale, osd_color, osd_thickness)
    cv2.putText(frame_copy, "MIS: " + str(data["mis"]), (10, 390),
                osd_font, osd_scale, osd_color, osd_thickness)
    cv2.putText(frame_copy, "VLD: " + str(data["vld"]).upper(), (1050, 390),
                osd_font, osd_scale, osd_color, osd_thickness)
    cv2.putText(frame_copy, "WPT: " + str(data["wpt"]), (10, 420),
                osd_font, osd_scale, osd_color, osd_thickness)
    cv2.putText(frame_copy, "PRC: " + str(data["prc"]).upper(), (1050, 420),
                osd_font, osd_scale, osd_color, osd_thickness)
    cv2.putText(frame_copy, "STG: " + str(data["stg"]), (10, 450),
                osd_font, osd_scale, osd_color, osd_thickness)
    if data["ptm"] == "1970-01-01 02:00:00":
        cv2.putText(frame_copy, "PTM: 00:00:00", (1050, 450),
                    osd_font, osd_scale, osd_color, osd_thickness)
    else:
        cv2.putText(frame_copy, "PTM: " + str(data["ptm"]).split(" ")[1], (1050, 450),
                    osd_font, osd_scale, osd_color, osd_thickness)
    cv2.putText(frame_copy, "TGT: " + str(data["tgt"]).upper(), (10, 510),
                osd_font, osd_scale, osd_color, osd_thickness)
    cv2.putText(frame_copy, "TLT: " + "{0:.6f}".format(data["tlt"]), (10, 540),
                osd_font, osd_scale, osd_color, osd_thickness)
    cv2.putText(frame_copy, "TLN: " + "{0:.6f}".format(data["tln"]), (10, 570),
                osd_font, osd_scale, osd_color, osd_thickness)
    cv2.putText(frame_copy, "TCF: " + str(data["tcf"]), (10, 600),
                osd_font, osd_scale, osd_color, osd_thickness)
    if data["ttm"] == "1970-01-01 02:00:00":
        cv2.putText(frame_copy, "TTM: 00:00:00", (10, 630),
                    osd_font, osd_scale, osd_color, osd_thickness)
    else:
        cv2.putText(frame_copy, "TTM: " + str(data["ttm"]).split(" ")[1], (10, 630),
                    osd_font, osd_scale, osd_color, osd_thickness)
    cv2.putText(frame_copy, data["cmt"], (10, 710),
                osd_font, osd_scale, osd_color, osd_thickness)
    cv2.putText(frame_copy, data["tim"].split(" ")[0], (1050, 710),
                osd_font, osd_scale, osd_color, osd_thickness)
    return frame_copy


# deal with the directory creations
try:
    mission_index_last = max([int(x[-1]) for x in os.listdir(path_imagery_global)])
except (ValueError, FileNotFoundError) as error:
    mission_index_last = -1
mission_name = "mission_" + str(mission_index_last)
path_imagery_mission = path_imagery_global + "mission_" + str(mission_index_last) + "/"
path_imagery_raw = path_imagery_mission + "sim/"

file_list = os.listdir(path_imagery_raw)
file_list.sort()

for file_name in file_list:
    path_frame = path_imagery_raw + file_name
    frame_original = cv2.imread(path_frame)
    data = telemetry_data_reader(path_frame)
    pprint.pprint(data, indent=8)

    # put telemetry data on image
    frame_copy = telemetry_writer(frame_original, data)

    cv2.imshow("Simulation Frame Capture Visualization", frame_copy)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

    if slow_down:
        time.sleep(delay_loop)
