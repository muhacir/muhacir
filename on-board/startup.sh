#!/bin/bash

# number of the vehicle starting from zero
id=0

# start proxy
export LOCALAPPDATA="LOCALAPPDATA"
export OPENBLAS_CORETYPE=ARMV8
printf "[ReScience] [INFO] Starting telemetry proxy script..."
mkdir -p /home/m/logs
cd /home/m/logs || exit 1
screen -S proxy_onboard_$((id)) -d -m bash -c "mavproxy.py --aircraft vehicle_$((id)) --force-connected --master=/dev/serial/by-id/usb-Silicon_Labs_CP2102_USB_to_UART_Bridge_Controller_0001-if00-port0,115200 --out=192.168.1.200:$((9000+$id*10)) --out=127.0.0.1:$((10000+$id*10)) --out=192.168.1.201:$((20000+$id*10)) --daemon"
printf "[ReScience] [INFO] Started telemetry proxy script."

# configurations for maximum speed
nvpmodel -m 0
jetson_clocks

# start scripts
screen -S image_capture_$((id)) -d -m bash -c "/usr/bin/python3 /home/m/muhacir/on-board/image_capture.py --vehicle=$((id))"
screen -S target_image_detector_$((id)) -d -m bash -c "/usr/bin/python3 /home/m/muhacir/on-board/target_image_detector.py --vehicle=$((id))"
screen -S target_data_client_$((id)) -d -m bash -c "/usr/bin/python3 /home/m/muhacir/on-board/target_data_client.py --vehicle=$((id))"

# master only scripts
if [ $id = 1 ]; then
   screen -S target_data_server -d -m bash -c "/usr/bin/python3 /home/m/muhacir/on-board/target_data_server.py"
   screen -S target_waypoint_uploader -d -m bash -c "/usr/bin/python3 /home/m/muhacir/on-board/target_waypoint_uploader.py"
fi
