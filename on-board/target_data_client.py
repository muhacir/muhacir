import os
import sys
import time
import threading
import click
import piexif
import piexif.helper
import json
import logging
import urllib.request
import shutil

# configurations
path_imagery_global = "/home/m/muhacir/imagery_data/"
output_save = True
directory_dump_wait_time = 10
directory_process_wait_time = 10
target_data_upload_wait_time = 10
main_loop_wait_time = 10
log_format = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
logger_file_name = "target_data_client"
logger_log_name = "target_data_client"
minimum_consequent_target_frame = 3
target_set_url = "http://192.168.1.201:8888/set_target"
target_set_request_header = {"content-type": "application/json"}
target_set_response_timeout = 10
target_logger_wait_time = 10

# global variables
frame_indexes_raw = []
frame_indexes_processed = []
frame_indexes_target = []
mission_index_current = 0
mission_name = ""
path_imagery_mission = ""
path_imagery_raw = ""
path_imagery_processed = ""
vehicle_name = ""
vehicle_number = 0
log = None
target_locations = []
frame_indexes_final = []


# logger utility for convenience
def logger(file_name="unknown_log", log_name="unknown_log"):
    # create log directory if not exists
    os.makedirs("logs", exist_ok=True)

    # basic configs for logger
    logging.basicConfig(level=logging.DEBUG, stream=sys.stdout, format=log_format)

    # create logger and set level
    my_logger = logging.getLogger(log_name)
    my_logger.setLevel(logging.DEBUG)

    # create handler
    handler = logging.FileHandler("logs/" + file_name + ".log")

    # create a logging format
    formatter = logging.Formatter(log_format)
    handler.setFormatter(formatter)

    # add the file handler to the logger
    my_logger.addHandler(handler)

    # return logger
    return my_logger


# exif dumper convenience function
def telemetry_data_reader(file_name):
    # try to read exif data
    try:

        # read the exif data
        exif_dict = piexif.load(file_name)

        # extract the serialized data
        user_comment = piexif.helper.UserComment.load(exif_dict["Exif"][piexif.ExifIFD.UserComment])

        # deserialize
        user_data = json.loads(user_comment)
        user_data = dict(user_data)

    # exception occurred
    except Exception as e:

        # log the error
        log.error(e)

        # fall back to empty dictionary
        user_data = {}

    # expose data
    return user_data


# dump frame indexes in the directory
def directory_dump():
    # get global variables
    global frame_indexes_raw, path_imagery_raw, log

    # do below always
    while True:

        # try to update frame raw index list
        try:
            # get file list
            file_list = os.listdir(path_imagery_raw)
            file_list.sort()

            # this loop will collect raw frame indexes periodically
            for file_name in file_list:
                frame_index = int(file_name[-10:-4])
                if frame_index not in frame_indexes_raw:
                    frame_indexes_raw.append(frame_index)

        # except all errors
        except Exception as e:

            # log the error
            log.error(e)

        # do below at all cases
        finally:

            # cool down
            time.sleep(directory_dump_wait_time)


# function to find targets in frames
def directory_process():
    # get global variables
    global path_imagery_raw, path_imagery_processed, vehicle_number, vehicle_name, mission_name, log
    global frame_indexes_raw, frame_indexes_processed, frame_indexes_target, directory_process_wait_time

    # do below always
    while True:

        # try to find the frames include target
        try:

            # for index in raw index list
            for frame_index in frame_indexes_raw:

                # if frame not processed before with this script
                if frame_index not in frame_indexes_processed:

                    # calculate frame path
                    frame_name = "{0}_{1}_frame_{2:06d}.jpg"
                    frame_name = frame_name.format(mission_name, vehicle_name, frame_index)
                    frame_path = path_imagery_raw + frame_name

                    # get exif data from frame image file
                    telemetry_data = telemetry_data_reader(frame_path)

                    # if exif data has telemetry data
                    if telemetry_data != {}:

                        # if frame is processed with target detector
                        if telemetry_data["prc"]:
                            # append the frame index to processed frame index list
                            frame_indexes_processed.append(frame_index)

                            # log the action
                            log.info("processed frame: " + frame_path)

                            # if frame has target in it
                            if telemetry_data["tgt"]:
                                # append target frame number to target frame indexes list
                                frame_indexes_target.append(frame_index)

                                # log the action
                                log.info("found target in: " + frame_path)

        # except all errors
        except Exception as e:

            # log the error
            log.error(e)

        # do below at all cases
        finally:

            # cool down
            time.sleep(directory_process_wait_time)


# calculate final target location and send it to server
def target_data_upload():
    global frame_indexes_target, minimum_consequent_target_frame, mission_name, vehicle_name, path_imagery_raw
    global target_set_url, target_set_response_timeout, log, target_locations, frame_indexes_final

    # find consequent frame those contain target
    while True:
        frame_indexes_counter = 1
        frame_indexes_final = []
        frame_indexes_final_temp = []
        if frame_indexes_target:
            frame_indexes_final_temp = [frame_indexes_target[0]]
            for i in range(1, len(frame_indexes_target)):
                if frame_indexes_target[i] == frame_indexes_final_temp[-1] + 1:
                    frame_indexes_final_temp.append(frame_indexes_target[i])
                    frame_indexes_counter += 1
                else:
                    if frame_indexes_counter >= minimum_consequent_target_frame:
                        break
                    else:
                        frame_indexes_final_temp = [frame_indexes_target[i]]

        # collected enough consequent target frames
        if frame_indexes_counter >= minimum_consequent_target_frame:

            # frame indexes those include target consequently
            frame_indexes_final = list(frame_indexes_final_temp)

            # brake the loop
            break

        # cool down the loop
        time.sleep(target_data_upload_wait_time)

    # get location information from consequent frames
    for frame_index in frame_indexes_final:
        # calculate frame path
        frame_name = "{0}_{1}_frame_{2:06d}.jpg"
        frame_name = frame_name.format(mission_name, vehicle_name, frame_index)
        frame_path = path_imagery_raw + frame_name

        # get exif data from frame image file
        telemetry_data = telemetry_data_reader(frame_path)
        target_locations.append({"latitude": telemetry_data["lat"], "longitude": telemetry_data["lon"]})

    # calculate final target location by averaging target locations
    final_target_location_latitude = sum([location["latitude"] for location in target_locations]) / len(
        target_locations)
    final_target_location_longitude = sum([location["longitude"] for location in target_locations]) / len(
        target_locations)
    final_target_location_latitude = float("{:.6f}".format(final_target_location_latitude))
    final_target_location_longitude = float("{:.6f}".format(final_target_location_longitude))
    final_target_location = {"latitude": final_target_location_latitude, "longitude": final_target_location_longitude}

    log.info("final target location: " + str(final_target_location))

    # send final target location to server
    while True:

        # make request to server
        target_set_request = urllib.request.Request(url=target_set_url,
                                                    data=json.dumps(final_target_location).encode("utf-8"),
                                                    headers=target_set_request_header)
        try:
            target_set_response = urllib.request.urlopen(target_set_request, timeout=target_set_response_timeout)
            target_set_response = json.loads(target_set_response.read())
        except Exception as e:
            target_set_response = {}

        if "latitude" in target_set_response.keys() and "longitude" in target_set_response.keys():
            break

        # cool down
        time.sleep(target_data_upload_wait_time)

    log.info("final target location sent to server: " + str(final_target_location))
    log.info("final target location from server: " + str(target_set_response))


# function that stores target logs for post flight log delivery
def target_logger():
    global frame_indexes_final, target_logger_wait_time, log
    global path_imagery_processed, path_imagery_raw, mission_name, vehicle_name

    while True:

        # if final target included frame indexes list is not empty
        if frame_indexes_final:
            for frame_index in frame_indexes_final:
                # calculate frame paths
                frame_name = "{0}_{1}_frame_{2:06d}.jpg"
                frame_name = frame_name.format(mission_name, vehicle_name, frame_index)
                frame_path_raw = path_imagery_raw + frame_name
                frame_path_processed = path_imagery_processed + frame_name

                # copy target included frames to processed folder
                try:

                    # copy the image frame
                    shutil.copy(frame_path_raw, frame_path_processed)

                    # copy the telemetry data
                    data = telemetry_data_reader(frame_path_raw)
                    with open(frame_path_processed + ".json", "w", encoding="utf-8") as file:
                        json.dump(data, file, ensure_ascii=False, indent=4)

                    # log the path
                    log.info("copied target included frame to: " + frame_path_processed)

                # error occurred
                except Exception as e:

                    # log the error
                    log.error(e)

            # break the loop because upload successful
            break

        # cool down
        time.sleep(target_logger_wait_time)


# main function
@click.command()
@click.option("--vehicle", help="ID of the vehicle", required=True, type=int)
def main(vehicle):
    # get global variables
    global mission_name, path_imagery_mission, path_imagery_raw, path_imagery_processed
    global mission_index_current, vehicle_number, vehicle_name, log

    # assign vehicle number
    vehicle_number = int(vehicle)

    # deal with the directory creations
    try:
        mission_index_current = max([int(x.split("_")[-1]) for x in os.listdir(path_imagery_global)])
    except (ValueError, FileNotFoundError) as e:
        mission_index_current = 0
    mission_name = "mission_" + str(mission_index_current)
    vehicle_name = "vehicle_" + str(vehicle_number)
    path_imagery_mission = path_imagery_global + "mission_" + str(mission_index_current) + "/"
    path_imagery_raw = path_imagery_mission + "raw/"
    path_imagery_processed = path_imagery_mission + "processed/"
    if output_save:
        os.makedirs(path_imagery_raw, exist_ok=True)
        os.makedirs(path_imagery_processed, exist_ok=True)

    # create logger instance
    log = logger(file_name=mission_name + "_" + vehicle_name + "_" + logger_file_name,
                 log_name=mission_name + "_" + vehicle_name + "_" + logger_log_name)

    # create threads
    directory_dump_thread = threading.Thread(target=directory_dump)
    directory_process_thread = threading.Thread(target=directory_process)
    target_data_upload_thread = threading.Thread(target=target_data_upload)
    target_logger_thread = threading.Thread(target=target_logger)

    # start threads
    directory_dump_thread.start()
    directory_process_thread.start()
    target_data_upload_thread.start()
    target_logger_thread.start()

    # dummy waiting
    while True:
        log.info("running...")
        time.sleep(main_loop_wait_time)


# run the main function
if __name__ == "__main__":
    main()
