import os
import sys
import time
import math
import datetime
import logging
import click
import cv2
import dronekit
import json
import piexif
import piexif.helper

# configurations
path_imagery_global = "/home/m/muhacir/imagery_data/"
path_imagery_source = 0
frame_width = 1280
frame_height = 720
frame_rate = 30
slow_down = True
delay_initialize = 5
delay_loop = 0.1
timeout_connection = 60 * 60 * 24
timeout_heartbeat = 60 * 60 * 24
logger_file_name = "image_capture"
logger_log_name = "image_capture"
output_save = True
vehicle_present = True
log_format = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
validation_conditions_arm = True
validation_conditions_mode = "AUTO"
validation_conditions_command = 4
validation_conditions_roll = 60.0
validation_conditions_pitch = 60.0
validation_conditions_altitude_minimum = 45.0
validation_conditions_altitude_maximum = 75.0


# logger utility for convenience
def logger(file_name="unknown_log", log_name="unknown_log"):
    # create log directory if not exists
    os.makedirs("logs", exist_ok=True)

    # basic configs for logger
    logging.basicConfig(level=logging.DEBUG, stream=sys.stdout, format=log_format)

    # create logger and set level
    my_logger = logging.getLogger(log_name)
    my_logger.setLevel(logging.DEBUG)

    # create handler
    handler = logging.FileHandler("logs/" + file_name + ".log")

    # create a logging format
    formatter = logging.Formatter(log_format)
    handler.setFormatter(formatter)

    # add the file handler to the logger
    my_logger.addHandler(handler)

    # return logger
    return my_logger


# validation function to decide the frame is valid
def frame_validation(my_vehicle):
    # build up conditions
    validation_conditions = [bool(my_vehicle.armed) == validation_conditions_arm,
                             str(my_vehicle.mode.name) == validation_conditions_mode,
                             int(my_vehicle.commands.next) >= validation_conditions_command,
                             abs(math.degrees(float(my_vehicle.attitude.roll))) <= validation_conditions_roll,
                             abs(math.degrees(float(my_vehicle.attitude.pitch))) <= validation_conditions_pitch,
                             validation_conditions_altitude_minimum <= float(
                                 my_vehicle.location.global_relative_frame.alt), float(
            my_vehicle.location.global_relative_frame.alt) <= validation_conditions_altitude_maximum]

    # construct validation
    validation = all(validation_conditions)

    # return to validation
    return validation


# telemetry data generator to save telemetry data in image name
def telemetry_data_generator(my_vehicle, my_mission, my_frame):
    # try to build up vehicular data
    try:

        # build up dictionary
        my_data = {"sid": int(my_vehicle.parameters["SYSID_THISMAV"]),  # id of the vehicle that captured this frame
                   "arm": bool(my_vehicle.armed),  # vehicle arm status
                   "mod": str(my_vehicle.mode.name),  # vehicle mode
                   "wpt": int(my_vehicle.commands.next),  # current waypoint number
                   "lat": float(my_vehicle.location.global_relative_frame.lat),  # latitude in degrees
                   "lon": float(my_vehicle.location.global_relative_frame.lon),  # longitude in degrees
                   "alt": float(my_vehicle.location.global_relative_frame.alt),  # altitude in meters
                   "tal": float(my_vehicle.altitude_terrain),  # terrain altitude in meters
                   "hdg": float(my_vehicle.heading),  # heading of the vehicle relative to north in degrees
                   "rll": math.degrees(float(my_vehicle.attitude.roll)),  # instantaneous roll angle in degrees
                   "ptc": math.degrees(float(my_vehicle.attitude.pitch)),  # instantaneous pitch angle in degrees
                   "yaw": math.degrees(float(my_vehicle.attitude.yaw)),  # instantaneous yaw angle in degrees
                   "gsp": float(my_vehicle.groundspeed),  # ground speed in m/s
                   "asp": float(my_vehicle.airspeed),  # air speed in m/s
                   "cur": float(my_vehicle.battery.current),  # battery current
                   "bat": float(my_vehicle.battery.level),  # battery level remaining
                   "vlt": float(my_vehicle.battery.voltage),  # battery voltage
                   "hbt": float(my_vehicle.last_heartbeat),  # delay between telemetry packets in seconds
                   "tim": str(my_vehicle.time_unix),  # frame capture timestamp
                   "tgt": False,  # target presence in this image
                   "tpx": -1,  # detected target pixel coordinate x
                   "tpy": -1,  # detected target pixel coordinate y
                   "tlt": 0.0,  # detected target global coordinate latitude in degrees
                   "tln": 0.0,  # detected target global coordinate longitude in degrees
                   "tcf": 0.0,  # detected target confidence level between 0-100
                   "ttm": str(datetime.datetime.fromtimestamp(0)),  # target detection timestamp
                   "mis": my_mission,  # current mission index
                   "frm": my_frame,  # current image frame counter
                   "stg": 0,  # current mission stage
                   "cmt": "AKHISAR",  # user comment string
                   "vld": frame_validation(my_vehicle),  # frame is valid and can be processed
                   "prc": False,  # this frame is processed before
                   "ptm": str(datetime.datetime.fromtimestamp(0)),  # processing timestamp
                   "sim": False,  # frame created in simulation
                   "btm": int(my_vehicle.time_boot)  # boot time in seconds
                   }

    # build up vehicular data is failed
    except Exception as e:
        my_data = {}

    # expose dictionary
    return my_data


# save telemetry data in image
def telemetry_data_writer(my_file, my_data):
    # try to embed vehicular data in image
    try:

        # load existing exif data from image
        exif_dict = piexif.load(my_file)

        # insert telemetry data in user comment field
        exif_dict["Exif"][piexif.ExifIFD.UserComment] = piexif.helper.UserComment.dump(json.dumps(my_data),
                                                                                       encoding="unicode")

        # insert mutated data (serialised into JSON) into image
        piexif.insert(piexif.dump(exif_dict), my_file)

    # failed to embed vehicular data in image
    except Exception as e:
        pass


# vehicle system time listener
def system_time_listener(self, name, message):
    # try to collect time
    try:
        unix_time = int(message.time_unix_usec / 1000000)
        boot_time = int(message.time_boot_ms / 1000)
        self.time_unix = datetime.datetime.fromtimestamp(unix_time)
        self.time_boot = boot_time

    # collect time failed
    except Exception as e:
        pass


# vehicle altitude listener
def terrain_listener(self, name, message):
    # try to read vehicle terrain altitude
    try:
        self.altitude_terrain = message.altitude_terrain

    # error reading terrain altitude
    except Exception as e:
        self.altitude_terrain = 0.0


# main function
@click.command()
@click.option("--vehicle", help="ID of the vehicle", required=True, type=int)
def main(vehicle):
    # assign vehicle number
    vehicle_number = int(vehicle)

    # vehicle connection path
    path_vehicle_connection = "udp:127.0.0.1:" + str(10000 + vehicle_number * 10)

    # connect to vehicle
    if vehicle_present:
        vehicle = dronekit.connect(path_vehicle_connection, wait_ready=True,
                                   timeout=timeout_connection, heartbeat_timeout=timeout_heartbeat)
        vehicle.commands.download()
        vehicle.commands.wait_ready()
        vehicle.add_message_listener("SYSTEM_TIME", system_time_listener)
        vehicle.add_message_listener("TERRAIN_REPORT", terrain_listener)
        vehicle.parameters["SYSID_THISMAV"] = vehicle_number
        vehicle.altitude_terrain = 0.0
        vehicle.time_unix = 0
        vehicle.time_boot = 0
        vehicle_name = "vehicle_" + str(vehicle_number)
    else:
        vehicle_name = "test"

    # deal with the directory creations
    try:
        mission_index_last = max([int(x.split("_")[-1]) for x in os.listdir(path_imagery_global)])
    except (ValueError, FileNotFoundError) as error:
        mission_index_last = -1
    mission_index_current = mission_index_last
    mission_name = "mission_" + str(mission_index_current)
    path_imagery_mission = path_imagery_global + "mission_" + str(mission_index_current) + "/"
    path_imagery_raw = path_imagery_mission + "raw/"
    if output_save:
        os.makedirs(path_imagery_raw, exist_ok=True)

    # create logger instance
    log = logger(file_name=mission_name + "_" + vehicle_name + "_" + logger_file_name,
                 log_name=mission_name + "_" + vehicle_name + "_" + logger_log_name)

    # frame counter
    frame_counter = -1

    # endless loop
    while True:

        # open image source
        log.info("trying to open source: " + str(path_imagery_source))
        image_source = cv2.VideoCapture(path_imagery_source, cv2.CAP_ANY)
        log.info("opened source: " + str(path_imagery_source))

        # initial configurations for camera
        log.info("configuring source: " + str(path_imagery_source))
        image_source.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'))
        image_source.set(cv2.CAP_PROP_FRAME_WIDTH, frame_width)
        image_source.set(cv2.CAP_PROP_FRAME_HEIGHT, frame_height)
        image_source.set(cv2.CAP_PROP_FPS, frame_rate)
        log.info("configured source: " + str(path_imagery_source))

        # cool down
        time.sleep(delay_initialize)

        # check if image source is opened
        while image_source.isOpened():

            # read the image frame
            success, frame_original = image_source.read()

            # image frame successfully read
            if success and output_save:

                # increase file counter
                frame_counter += 1

                # generate file name
                frame_name = "frame_" + "{:06d}".format(frame_counter)
                frame_name_with_extension = frame_name + ".jpg"
                file_name = mission_name + "_" + vehicle_name + "_" + frame_name_with_extension
                path_frame = path_imagery_raw + file_name
                log.info("frame captured successfully: " + frame_name)

                # save the frame
                cv2.imwrite(path_frame, frame_original)
                log.info("frame saved to:" + path_frame)

                # embed telemetry data to image if vehicle is present
                if vehicle_present:
                    # get telemetry data
                    telemetry_data = telemetry_data_generator(vehicle, mission_index_current, frame_counter)
                    log.info("generated telemetry metadata: " + vehicle_name)

                    # write telemetry data inside image
                    telemetry_data_writer(path_frame, telemetry_data)
                    log.info("telemetry metadata saved to: " + path_frame)

            # image frame can not be read
            else:

                # release camera if can not be opened
                log.info("error reading frame")
                image_source.release()
                log.info("released source")

                # break deep loop
                break

            # slow down the process if needed
            if slow_down:
                time.sleep(delay_loop)

        # release camera if can not be opened
        log.info("error reading frame")
        image_source.release()
        log.info("released source")


# main function
if __name__ == "__main__":
    main()
