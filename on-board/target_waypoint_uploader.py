import os
import sys
import logging
import concurrent.futures
import time
from pymavlink import mavutil
from pymavlink.dialects.v20 import all as mavlink2
import urllib.request
import json
import geopy.distance

# configurations
vehicle_count = 5
vehicle_indexes = [5, 4, 3, 2, 1]
connection_strings = ["udp:192.168.1.201:" + str(20000 + i * 10) for i in range(vehicle_count)]
target_locations = [{"latitude": 0, "longitude": 0},
                    {"latitude": 0, "longitude": 0},
                    {"latitude": 0, "longitude": 0},
                    {"latitude": 0, "longitude": 0}]
radius_loiter_turn = -60.0
flight_level_default = 60.0
mavlink_connection_timeout = 5
mavlink_connection_maximum_retry = 10
target_get_url = "http://0.0.0.0:8888/get_target"
log_format = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
logger_file_name = "target_waypoint_uploader"
logger_log_name = "target_waypoint_uploader"
path_imagery_global = "/home/m/muhacir/imagery_data/"
loop_cool_down_wait_time = 1
target_start_index = 26
target_end_index = 29
center_waypoint_index = 24
target_complete_index = 30

# global variables
log = None


# logger utility for convenience
def logger(file_name="unknown_log", log_name="unknown_log"):
    # create log directory if not exists
    os.makedirs("logs", exist_ok=True)

    # basic configs for logger
    logging.basicConfig(level=logging.DEBUG, stream=sys.stdout, format=log_format)

    # create logger and set level
    my_logger = logging.getLogger(log_name)
    my_logger.setLevel(logging.DEBUG)

    # create handler
    handler = logging.FileHandler("logs/" + file_name + ".log")

    # create a logging format
    formatter = logging.Formatter(log_format)
    handler.setFormatter(formatter)

    # add the file handler to the logger
    my_logger.addHandler(handler)

    # return logger
    return my_logger


# send target locations to the vehicle
def send_target(connection_string, locations, vehicle_number):
    global log, loop_cool_down_wait_time, mavlink_connection_maximum_retry

    # send the list until received acknowledgement
    for _ in range(mavlink_connection_maximum_retry):

        # try to send the mission items
        try:

            # create connection
            log.info("trying to connect to: " + connection_string)
            connection = mavutil.mavlink_connection(connection_string)

            # wait for the heartbeat
            connection.wait_heartbeat(timeout=mavlink_connection_timeout)
            log.info("connected to: " + connection_string)

            # create partial waypoint list request message
            msg = mavlink2.MAVLink_mission_write_partial_list_message(target_system=connection.target_system,
                                                                      target_component=connection.target_component,
                                                                      start_index=target_start_index,
                                                                      end_index=target_end_index,
                                                                      mission_type=0)

            # send partial waypoint list request message
            connection.mav.send(msg)
            log.info("sent partial waypoint list request message to: " + connection_string)

            # send all the target locations
            for i in range(target_start_index, target_end_index + 1):
                # wait for the mission item request from vehicle
                msg = connection.recv_match(type=["MISSION_REQUEST"], blocking=True, timeout=mavlink_connection_timeout)
                log.info(msg)
                log.info("got mission item request from: " + connection_string)

                # create mission item message
                msg = mavlink2.MAVLink_mission_item_message(target_system=connection.target_system,
                                                            target_component=connection.target_component,
                                                            seq=msg.seq,
                                                            frame=mavlink2.MAV_FRAME_GLOBAL_RELATIVE_ALT,
                                                            command=mavlink2.MAV_CMD_NAV_LOITER_TURNS,
                                                            current=0,
                                                            autocontinue=0,
                                                            param1=1,
                                                            param2=0,
                                                            param3=radius_loiter_turn,
                                                            param4=0,
                                                            x=locations[i - target_start_index]["latitude"],
                                                            y=locations[i - target_start_index]["longitude"],
                                                            z=flight_level_default)

                # send mission item message
                connection.mav.send(msg)
                log.info("sent mission item to: " + connection_string)

            # wait for the mission item list sending complete message
            msg = connection.recv_match(type=["MISSION_ACK"], blocking=True, timeout=mavlink_connection_timeout)

            # log the message
            log.info(msg)
            log.info("partial mission item list uploaded to: " + connection_string)

            # if list uploaded break the loop
            if msg.get_type() == "MISSION_ACK":
                # close the connection
                connection.close()
                log.info("closed connection from: " + connection_string)

                # brake the loop
                break

        # except all errors
        except Exception as e:

            # print the error
            log.error(e)

        # cool down the loop
        time.sleep(loop_cool_down_wait_time)


def goto_center(connection_string, vehicle_number):
    global log, loop_cool_down_wait_time, mavlink_connection_maximum_retry

    # do below always until the condition(s) met
    for _ in range(mavlink_connection_maximum_retry):
        # try to send the mission items
        try:

            # create connection
            log.info("trying to connect to: " + connection_string)
            connection = mavutil.mavlink_connection(connection_string)

            # wait for the heartbeat
            connection.wait_heartbeat(timeout=mavlink_connection_timeout)
            log.info("connected to: " + connection_string)

            # create set current mission item message
            msg = mavlink2.MAVLink_mission_set_current_message(target_system=connection.target_system,
                                                               target_component=connection.target_component,
                                                               seq=center_waypoint_index)

            # send set current mission item message
            connection.mav.send(msg)
            log.info("sent set current mission item to: " + connection_string)

            # wait for mission item current message
            msg = connection.recv_match(type=["MISSION_CURRENT"], blocking=True, timeout=mavlink_connection_timeout)
            log.info(msg)

            if msg.get_type() == "MISSION_CURRENT":
                if msg.seq == center_waypoint_index:
                    # close the connection
                    connection.close()
                    log.info("closed connection from: " + connection_string)

                    # brake the loop
                    break

        # except all errors
        except Exception as e:

            # print the error
            log.error(e)

        # cool down the loop
        time.sleep(loop_cool_down_wait_time)


# send targets to vehicle in parallel
def send_targets():
    global connection_strings, target_locations
    with concurrent.futures.ThreadPoolExecutor(max_workers=vehicle_count) as executor:
        return [executor.submit(send_target, connection_string, target_locations, vehicle_number) for
                vehicle_number, connection_string in enumerate(connection_strings)]


# send vehicles to center locations
def goto_centers():
    global connection_strings
    with concurrent.futures.ThreadPoolExecutor(max_workers=vehicle_count) as executor:
        return [executor.submit(goto_center, connection_string, vehicle_number) for
                vehicle_number, connection_string in enumerate(connection_strings)]


# main function
def main():
    global log, path_imagery_global, loop_cool_down_wait_time

    # deal with the directory creations
    try:
        mission_index_current = max([int(x.split("_")[-1]) for x in os.listdir(path_imagery_global)])
    except (ValueError, FileNotFoundError) as e:
        mission_index_current = 0

    # create logger instance
    log = logger(file_name=logger_file_name, log_name=logger_log_name)

    # do below always
    while True:

        # try to get target data
        try:

            # get the data
            response = urllib.request.urlopen(target_get_url)
            target_data = json.loads(response.read())

            # log the data
            log.info("Got target data from server: " + str(target_data))

            # brake the loop if got the data
            if "latitude" in target_data.keys() and "longitude" in target_data.keys():

                # brake the loop
                break

        # catch all errors
        except Exception as e:

            # log the error
            log.error(e)

        # cool down the loop
        time.sleep(loop_cool_down_wait_time)

    # send vehicles to center location
    goto_centers()

    # calculate target locations
    for i in range(len(target_locations)):
        target_location = geopy.distance.distance(meters=abs(radius_loiter_turn)).destination(
            (target_data["latitude"], target_data["longitude"]), i * 90)
        target_locations[i]["latitude"] = target_location.latitude
        target_locations[i]["longitude"] = target_location.longitude

    # send target data to vehicles
    send_targets()

    # send vehicles to targets
    while True:

        # all vehicles deployed
        if not vehicle_indexes:
            break

        # get connection endpoint
        vehicle_index = vehicle_indexes[0]
        connection_string = connection_strings[vehicle_index - 1]

        # do below always until the condition(s) met
        while True:
            # try to send the mission items
            try:

                # create connection
                log.info("trying to connect to: " + connection_string)
                connection = mavutil.mavlink_connection(connection_string)

                # wait for the heartbeat
                connection.wait_heartbeat(timeout=mavlink_connection_timeout)
                log.info("connected to: " + connection_string)

                # create mission item message
                msg = mavlink2.MAVLink_mission_set_current_message(target_system=connection.target_system,
                                                                   target_component=connection.target_component,
                                                                   seq=center_waypoint_index + 1)

                # send mission item message
                connection.mav.send(msg)
                log.info("sent go to target location to: " + connection_string)

                # wait for the mission item list sending complete message
                msg = connection.recv_match(type=["MISSION_CURRENT"], blocking=True, timeout=mavlink_connection_timeout)
                log.info(msg)

                if msg.get_type() == "MISSION_CURRENT":
                    if msg.seq == center_waypoint_index + 1:
                        log.info("verified go to target location to: " + connection_string)

                        # close the connection
                        connection.close()
                        log.info("closed connection from: " + connection_string)

                        # brake the loop
                        break

            # except all errors
            except Exception as e:

                # print the error
                log.error(e)

            # cool down the loop
            time.sleep(loop_cool_down_wait_time)

        # do below always until the condition(s) met
        while True:
            # try to read mission current message
            try:

                # create connection
                log.info("trying to connect to: " + connection_string)
                connection = mavutil.mavlink_connection(connection_string)

                # wait for the heartbeat
                connection.wait_heartbeat(timeout=mavlink_connection_timeout)
                log.info("connected to: " + connection_string)

                # wait for the mission current message
                msg = connection.recv_match(type=["MISSION_CURRENT"], blocking=True,
                                            timeout=mavlink_connection_timeout)
                log.info(msg)
                log.info("got mission current message from: " + connection_string)

                # if got mission current message
                if msg.get_type() == "MISSION_CURRENT":

                    # if vehicle flying towards approach location
                    if msg.seq == target_complete_index:
                        log.info("verified mission complete from: " + connection_string)

                        # delete vehicle index because vehicle completed the mission successfully
                        del vehicle_indexes[0]

                        # close the connection
                        connection.close()
                        log.info("closed connection from: " + connection_string)

                        # brake the loop
                        break

            # except all errors
            except Exception as e:

                # print the error
                log.error(e)

            # cool down the loop
            time.sleep(loop_cool_down_wait_time)

        # cool down the loop
        time.sleep(loop_cool_down_wait_time)

    # endless loop
    while True:

        # print success message
        log.info("mission complete!")

        # cool down the loop
        time.sleep(loop_cool_down_wait_time)


# main function
if __name__ == "__main__":
    main()
