import os
import sys
import time
import datetime
import cv2
import pycuda.autoinit
import piexif
import piexif.helper
import json
from target_image_detector_utilities import TrtYOLO
import math
import geopy.distance
import click
import logging

# configurations
model = "/home/m/muhacir/on-board/yolov4-tiny-custom.trt"
category_num = 1
letter_box = False
confidence_threshold = 0.3
minimum_accuracy = 0.75
maximum_localization_error = 60.0
frame_width = 1280
frame_height = 720
pixel_per_meter = 23.5
source_wait_time = 0.1
path_imagery_global = "/home/m/muhacir/imagery_data/"
log_format = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
logger_file_name = "target_detector"
logger_log_name = "target_detector"


# logger utility for convenience
def logger(file_name="unknown_log", log_name="unknown_log"):
    # create log directory if not exists
    os.makedirs("logs", exist_ok=True)

    # basic configs for logger
    logging.basicConfig(level=logging.DEBUG, stream=sys.stdout, format=log_format)

    # create logger and set level
    my_logger = logging.getLogger(log_name)
    my_logger.setLevel(logging.DEBUG)

    # create handler
    handler = logging.FileHandler("logs/" + file_name + ".log")

    # create a logging format
    formatter = logging.Formatter(log_format)
    handler.setFormatter(formatter)

    # add the file handler to the logger
    my_logger.addHandler(handler)

    # return logger
    return my_logger


# exif dumper convenience function
def telemetry_data_reader(file_name):
    # try to read exif data
    try:

        # read the exif data
        exif_dict = piexif.load(file_name)

        # extract the serialized data
        user_comment = piexif.helper.UserComment.load(exif_dict["Exif"][piexif.ExifIFD.UserComment])

        # deserialize
        user_data = json.loads(user_comment)
        user_data = dict(user_data)

    # exception occurred
    except Exception as e:

        # print the error
        print(e)

        # fall back to empty dictionary
        user_data = {}

    # expose data
    return user_data


# save telemetry data in image
def telemetry_data_writer(my_file, my_data):
    # try to write exif data
    try:

        # load existing exif data from image
        exif_dict = piexif.load(my_file)

        # insert telemetry data in user comment field
        exif_dict["Exif"][piexif.ExifIFD.UserComment] = piexif.helper.UserComment.dump(json.dumps(my_data),
                                                                                       encoding="unicode")

        # insert mutated data (serialised into JSON) into image
        piexif.insert(piexif.dump(exif_dict), my_file)

    # exception occurred
    except Exception as e:

        # print the error
        print(e)


# calculate angle between three coordinates
def calculate_angle(a, b, c):
    angle = math.degrees(math.atan2(c[1] - b[1], c[0] - b[0]) - math.atan2(a[1] - b[1], a[0] - b[0]))
    return angle + 360.0 if angle < 0.0 else angle


# calculate real world target vector based on imagery data
def calculate_target_vector(target_coordinate_midpoint_x, target_coordinate_midpoint_y, vehicle_position_heading):
    # image frame midpoint
    vehicle_coordinate_x = int(frame_width / 2.0)
    vehicle_coordinate_y = int(frame_height / 2.0)

    # calculate pixel vector length between image frame midpoint and target midpoint
    target_vector_length = math.sqrt(math.pow(target_coordinate_midpoint_x - vehicle_coordinate_x, 2) + math.pow(
        target_coordinate_midpoint_y - vehicle_coordinate_y, 2))

    target_vector_length = target_vector_length / pixel_per_meter

    # calculate pixel vector bearing between image frame midpoint and target midpoint
    target_vector_bearing = 90 + calculate_angle((target_coordinate_midpoint_x, target_coordinate_midpoint_y),
                                                 (vehicle_coordinate_x, vehicle_coordinate_y),
                                                 (vehicle_coordinate_x + 10,
                                                  vehicle_coordinate_y)) + vehicle_position_heading

    # return to target vector length and bearing
    return target_vector_length, target_vector_bearing


# calculate target location based on vehicle location, target distance and bearing
def calculate_target_position(distance, latitude, longitude, bearing):
    # calculate target locations
    target_location = geopy.distance.distance(meters=distance).destination((latitude, longitude), bearing)

    # return to target location latitude longitude tuple
    return target_location.latitude, target_location.longitude


# main function
@click.command()
@click.option("--vehicle", help="ID of the vehicle", required=True, type=int)
def main(vehicle):
    # assign vehicle number
    vehicle_number = int(vehicle)

    # deal with getting directory number
    try:
        mission_index_current = max([int(x.split("_")[-1]) for x in os.listdir(path_imagery_global)])
    except (ValueError, FileNotFoundError) as error:
        mission_index_current = 0
    mission_name = "mission_" + str(mission_index_current)
    vehicle_name = "vehicle_" + str(vehicle_number)
    path_imagery_mission = path_imagery_global + "mission_" + str(mission_index_current) + "/"
    path_imagery_raw = path_imagery_mission + "raw/"

    # create logger instance
    log = logger(file_name=mission_name + "_" + vehicle_name + "_" + logger_file_name,
                 log_name=mission_name + "_" + vehicle_name + "_" + logger_log_name)

    # load model
    trt_yolo = TrtYOLO(model, category_num, letter_box)
    log.info("Loaded model!")

    # initiate fps counter
    fps = 0.0
    tic = time.time()

    # start detection loop
    m = 0
    while True:

        # create frame path and variables
        frame_name = "{0}_{1}_frame_{2:06}.jpg".format(mission_name, vehicle_name, m)
        frame_path = path_imagery_raw + frame_name
        data = {}
        frame = None

        # try to process image
        try:

            # wait for the new frame
            while frame is None:
                # read frame
                frame = cv2.imread(frame_path)

                # break loop if frame captured
                if frame is not None:
                    break

                # trying to read image frame
                log.info("trying to read image frame: " + frame_path)

                # wait for the new frame
                time.sleep(source_wait_time)

            # read image successfully
            log.info("read image frame: " + frame_path)

            # wait for the new data
            while data == {}:

                # read telemetry data from image
                data = telemetry_data_reader(frame_path)

                # break loop telemetry data found
                if data != {}:
                    break

                # trying to read telemetry data
                log.info("trying to read telemetry data: " + frame_path)

                # wait for the new data
                time.sleep(source_wait_time)

            # read telemetry data successfully
            log.info("read telemetry data: " + frame_path)

            # frame will be processed
            data["prc"] = True
            data["ptm"] = str(datetime.datetime.now())

            # check if frame is valid
            if data["vld"]:

                # perform detection
                boxes, accuracy, classes = trt_yolo.detect(frame, confidence_threshold)
                boxes, accuracy, classes = boxes.tolist(), accuracy.tolist(), classes.tolist()

                # calculate fps
                toc = time.time()
                curr_fps = 1.0 / (toc - tic)
                fps = curr_fps if fps == 0.0 else (fps * 0.95 + curr_fps * 0.05)
                tic = toc

                # there is a detection with accuracy
                if accuracy:

                    # accuracy is enough
                    if accuracy[0] > minimum_accuracy:
                        # vehicle location
                        vehicle_location_latitude = float(data["lat"])
                        vehicle_location_longitude = float(data["lon"])
                        vehicle_heading = float(data["hdg"])

                        # calculate pixel midpoint of the target in image
                        target_midpoint_x = int(int(boxes[0][0]) + (int(boxes[0][2]) - int(boxes[0][0])) / 2.0)
                        target_midpoint_y = int(int(boxes[0][1]) + (int(boxes[0][3]) - int(boxes[0][1])) / 2.0)

                        # calculate target distance and bearing vector in real life
                        distance, bearing = calculate_target_vector(target_midpoint_x, target_midpoint_y,
                                                                    vehicle_heading)

                        # calculate target position based on target vector
                        target_location = calculate_target_position(distance, vehicle_location_latitude,
                                                                    vehicle_location_longitude, bearing)

                        # calculate target distance for validating target location
                        target_distance = geopy.distance.geodesic(
                            (vehicle_location_latitude, vehicle_location_longitude),
                            (target_location[0], target_location[1])).meters

                        # validate target location
                        if target_distance > maximum_localization_error:
                            # fall back to vehicle location
                            target_location = (vehicle_location_latitude, vehicle_location_longitude)

                        # save target information on image
                        data["tgt"] = True
                        data["ttm"] = str(datetime.datetime.now())
                        data["tpx"] = int(target_midpoint_x)
                        data["tpy"] = int(target_midpoint_y)
                        data["tcf"] = float(accuracy[0])
                        data["tlt"] = float(target_location[0])
                        data["tln"] = float(target_location[1])

                        # log the target
                        log.info("found target on image frame: " + frame_path)

            # update telemetry data on image
            telemetry_data_writer(frame_path, data)

            # log the processing
            log.info("processed image frame: " + frame_path)

        # capture all errors
        except Exception as e:
            print(e)

        # increase loop counter
        m = m + 1


# main function
if __name__ == '__main__':
    main()
