import flask

app = flask.Flask(__name__)
target_data = {}


@app.route("/get_target")
def get_target():
    global target_data
    return flask.jsonify(target_data)


@app.route("/set_target", methods=["POST"])
def set_target():
    global target_data
    try:
        data = flask.request.json
    except Exception as e:
        data = {}
    if "latitude" in data.keys() and "longitude" in data.keys() and target_data == {}:
        target_data = data
        print("Got target data:", target_data)
    return flask.jsonify(data)


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8888)
