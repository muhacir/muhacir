#!/bin/bash

cd /home/m/muhacir/ground/missions || exit 1

for i in {0..4}
do
	screen -S wploader$((i + 1)) -d -m bash -c "mavproxy.py --master 127.0.0.1:$((30010 + $i*10)) --cmd='set requireexit true'"
	sleep 1
done
for i in {0..4}
do
	screen -S wploader$((i + 1)) -X stuff "wp load $(($i + 1)).waypoints^M"
	sleep 1
done
for i in {0..4}
do
	screen -xS wploader$((i + 1)) -X stuff "exit^M"
	sleep 1
done
for i in {0..4}
do
	screen -xS wploader$((i + 1)) -X stuff "^M"
	sleep 1
done
rm -r mav*