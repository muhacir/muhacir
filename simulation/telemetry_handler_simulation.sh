#!/bin/bash

cd /home/m/muhacir/simulation/logs || exit 1

for i in {0..4}
do
	screen -L -Logfile simulation_proxy$((i + 1)).log -S simulation_proxy$((i + 1)) -d -m bash -c "mavproxy.py --aircraft uav$((i + 1)) --master tcp:127.0.0.1:$((5760 + $i*10)) --out udp:127.0.0.1:$((9010 + $i*10)) --daemon"
	sleep 1
done
