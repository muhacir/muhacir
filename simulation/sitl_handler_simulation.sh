#!/bin/bash

declare -a lats
declare -a lons

lats=(39.881886 39.881886 39.881886 39.881886 39.881886 39.881886 39.881886 39.881886 39.881886 39.881886 39.881886 39.881886)
lons=(30.454059 30.454059 30.454059 30.454059 30.454059 30.454059 30.454059 30.454059 30.454059 30.454059 30.454059 30.454059)

cd /home/m/muhacir/simulation/logs || exit 1

for i in {0..4}
do
	screen -S simulation_firmware$((i + 1)) -d -m bash -c "$HOME/muhacir/simulation/arduplane -S -I$(($i + 0)) --model plane --speedup 1 --home ${lats[i]},${lons[i]},0,0  --defaults $HOME/muhacir/simulation/defaults/arduplane$((i + 1)).parm"
	sleep 1
done
